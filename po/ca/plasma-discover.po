# Translation of plasma-discover.po to Catalan
# Copyright (C) 2012-2023 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Orestes Mas <orestes@tsc.upc.edu>, 2012.
# Josep M. Ferrer <txemaq@gmail.com>, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2022, 2023
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-20 00:38+0000\n"
"PO-Revision-Date: 2023-12-20 09:09+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#: discover/DiscoverObject.cpp:190
#, kde-format
msgctxt "@title %1 is the distro name"
msgid ""
"%1 is not configured for installing apps through Discover—only app add-ons"
msgstr ""
"%1 no està configurat per a instal·lar aplicacions mitjançant el Discover, "
"només aplicacions i complements"

#: discover/DiscoverObject.cpp:192
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the distro name"
msgid ""
"To use Discover for apps, install your preferred module on the "
"<interface>Settings</interface> page, under <interface>Missing Backends</"
"interface>."
msgstr ""
"Per a utilitzar el Discover per a les aplicacions, instal·leu el mòdul "
"preferit a la pàgina <interface>Configuració</interface>, sota "
"<interface>Dorsals que manquen</interface>."

#: discover/DiscoverObject.cpp:195
#, kde-format
msgctxt "@action:button %1 is the distro name"
msgid "Report This Issue to %1"
msgstr "Informa del problema a %1"

#: discover/DiscoverObject.cpp:200
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is the distro name; in this case it always contains 'Arch "
"Linux'"
msgid ""
"To use Discover for apps, install <link url='https://wiki.archlinux.org/"
"title/Flatpak#Installation'>Flatpak</link> or <link url='https://wiki."
"archlinux.org/title/KDE#Discover_does_not_show_any_applications'>PackageKit</"
"link> using the <command>pacman</command> package manager.<nl/><nl/> Review "
"<link url='https://archlinux.org/packages/extra/x86_64/discover/'>%1's "
"packaging for Discover</link>"
msgstr ""
"Per a usar el Discover per a les aplicacions, instal·leu el <link "
"url='https://wiki.archlinux.org/title/Flatpak#Installation'>Flatpak</link> o "
"el <link url='https://wiki.archlinux.org/title/"
"KDE#Discover_does_not_show_any_applications'>PackageKit</link> fent servir "
"el gestor de paquets <command>pacman</command>.<nl/><nl/> Reviseu el <link "
"url='https://archlinux.org/packages/extra/x86_64/discover/'>sistema de "
"paquets de %1 per al Discover</link>"

#: discover/DiscoverObject.cpp:291
#, kde-format
msgid "Could not find category '%1'"
msgstr "No s'ha pogut trobar la categoria «%1»"

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr "Esteu intentant obrir el fitxer inexistent «%1»"

#: discover/DiscoverObject.cpp:328
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""
"No es pot interactuar amb els recursos del Flatpak sense el dorsal del "
"«flatpak» %1. Si us plau, primer instal·leu-lo."

#: discover/DiscoverObject.cpp:332
#, kde-format
msgid "Could not open %1"
msgstr "No s'ha pogut obrir %1"

#: discover/DiscoverObject.cpp:394
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr "Assegureu-vos que està instal·lat el suport de Snap"

#: discover/DiscoverObject.cpp:396
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""
"No s'ha pogut obrir %1 perquè no s'ha trobat en cap dels repositoris de "
"programari disponibles."

#: discover/DiscoverObject.cpp:399
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr "Informeu d'aquest problema als empaquetadors de la vostra distribució."

#: discover/DiscoverObject.cpp:402
#, kde-format
msgid "Report This Issue"
msgstr "Informa d'aquest problema"

#: discover/DiscoverObject.cpp:464 discover/DiscoverObject.cpp:466
#: discover/main.cpp:117
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/DiscoverObject.cpp:467
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""
"El Discover es va tancar abans d'acabar determinades tasques, espereu que "
"finalitzi."

#: discover/main.cpp:33
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr "Obre directament l'aplicació especificada pel seu URI appstream://."

#: discover/main.cpp:34
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr ""
"Obre amb una cerca per als programes que es poden gestionar amb el tipus "
"MIME donat."

#: discover/main.cpp:35
#, kde-format
msgid "Display a list of entries with a category."
msgstr "Mostra una llista d'entrades amb una categoria."

#: discover/main.cpp:36
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""
"Obre el Discover en un mode indicat. Els modes es corresponen als botons de "
"la barra d'eines."

#: discover/main.cpp:37
#, kde-format
msgid "List all the available modes."
msgstr "Llista tots els modes disponibles."

#: discover/main.cpp:38
#, kde-format
msgid "Local package file to install"
msgstr "Fitxer local de paquet a instal·lar"

#: discover/main.cpp:39
#, kde-format
msgid "List all the available backends."
msgstr "Llista tots els dorsals disponibles."

#: discover/main.cpp:40
#, kde-format
msgid "Search string."
msgstr "Cadena de la cerca."

#: discover/main.cpp:41
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Llista les opcions disponibles per als comentaris dels usuaris"

#: discover/main.cpp:43
#, kde-format
msgid "Supports appstream: url scheme"
msgstr "Implementa l'esquema d'URL «appstream:»"

#: discover/main.cpp:119
#, kde-format
msgid "An application explorer"
msgstr "Un explorador d'aplicacions"

#: discover/main.cpp:121
#, kde-format
msgid "© 2010-2024 Plasma Development Team"
msgstr "© 2010-2024, L'equip de desenvolupament del Plasma"

#: discover/main.cpp:122
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:123
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:124
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "Control de qualitat, disseny i usabilitat"

#: discover/main.cpp:128
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "Dan Leinir Turthra Jensen"

#: discover/main.cpp:129
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:136
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Orestes Mas"

#: discover/main.cpp:136
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "orestes@tsc.upc.edu"

#: discover/main.cpp:149
#, kde-format
msgid "Available backends:\n"
msgstr "Dorsals disponibles:\n"

#: discover/main.cpp:202
#, kde-format
msgid "Available modes:\n"
msgstr "Modes disponibles:\n"

#: discover/qml/AddonsView.qml:20 discover/qml/Navigation.qml:58
#, kde-format
msgid "Addons for %1"
msgstr "Complements per a %1"

#: discover/qml/AddonsView.qml:62
#, kde-format
msgid "More…"
msgstr "Més…"

#: discover/qml/AddonsView.qml:71
#, kde-format
msgid "Apply Changes"
msgstr "Aplica els canvis"

#: discover/qml/AddonsView.qml:79
#, kde-format
msgid "Reset"
msgstr "Restableix"

#: discover/qml/AddSourceDialog.qml:21
#, kde-format
msgid "Add New %1 Repository"
msgstr "Afegeix un repositori nou %1"

#: discover/qml/AddSourceDialog.qml:45
#, kde-format
msgid "Add"
msgstr "Afegeix"

#: discover/qml/AddSourceDialog.qml:50 discover/qml/DiscoverWindow.qml:271
#: discover/qml/InstallApplicationButton.qml:46
#: discover/qml/ProgressView.qml:139 discover/qml/SourcesPage.qml:201
#: discover/qml/UpdatesPage.qml:259 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "Cancel·la"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:224
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 valoració"
msgstr[1] "%1 valoracions"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:224
#, kde-format
msgid "No ratings yet"
msgstr "Encara no hi ha cap valoració"

#: discover/qml/ApplicationPage.qml:67
#, kde-format
msgctxt ""
"@item:inlistbox %1 is the name of an app source e.g. \"Flathub\" or \"Ubuntu"
"\""
msgid "From %1"
msgstr "Des de %1"

#: discover/qml/ApplicationPage.qml:84
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:203
#, kde-format
msgid "Unknown author"
msgstr "Autor desconegut"

#: discover/qml/ApplicationPage.qml:248
#, kde-format
msgid "Version:"
msgstr "Versió:"

#: discover/qml/ApplicationPage.qml:260
#, kde-format
msgid "Size:"
msgstr "Mida:"

#: discover/qml/ApplicationPage.qml:272
#, kde-format
msgid "License:"
msgid_plural "Licenses:"
msgstr[0] "Llicència:"
msgstr[1] "Llicències:"

#: discover/qml/ApplicationPage.qml:280
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr "Desconeguda"

#: discover/qml/ApplicationPage.qml:314
#, kde-format
msgid "What does this mean?"
msgstr "Què vol dir això?"

#: discover/qml/ApplicationPage.qml:323
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] "Vegeu-ne més…"
msgstr[1] "Vegeu-ne més…"

#: discover/qml/ApplicationPage.qml:334
#, kde-format
msgid "Content Rating:"
msgstr "Valoració del contingut:"

#: discover/qml/ApplicationPage.qml:343
#, kde-format
msgid "Age: %1+"
msgstr "Edat: %1+"

#: discover/qml/ApplicationPage.qml:363
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr "Veure els detalls…"

#: discover/qml/ApplicationPage.qml:385
#, kde-format
msgctxt "@info placeholder message"
msgid "Screenshots not available for %1"
msgstr "No hi ha captures de pantalla disponibles per a %1"

#: discover/qml/ApplicationPage.qml:559
#, kde-format
msgid "Documentation"
msgstr "Documentació"

#: discover/qml/ApplicationPage.qml:560
#, kde-format
msgid "Read the project's official documentation"
msgstr "Llegiu la documentació oficial del projecte"

#: discover/qml/ApplicationPage.qml:576
#, kde-format
msgid "Website"
msgstr "Lloc web"

#: discover/qml/ApplicationPage.qml:577
#, kde-format
msgid "Visit the project's website"
msgstr "Visiteu el lloc web del projecte"

#: discover/qml/ApplicationPage.qml:593
#, kde-format
msgid "Addons"
msgstr "Complements"

#: discover/qml/ApplicationPage.qml:594
#, kde-format
msgid "Install or remove additional functionality"
msgstr "Instal·leu o elimineu característiques addicionals"

#: discover/qml/ApplicationPage.qml:613
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr "Comparteix"

# NdT: Exports the application's URL to an external service
#: discover/qml/ApplicationPage.qml:614
#, kde-format
msgid "Send a link for this application"
msgstr "Envia un enllaç d'aquesta aplicació"

#: discover/qml/ApplicationPage.qml:630
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr "Doneu un cop d'ull a l'aplicació %1!"

#: discover/qml/ApplicationPage.qml:650
#, kde-format
msgid "What's New"
msgstr "Què hi ha de nou"

#: discover/qml/ApplicationPage.qml:680
#, kde-format
msgid "Reviews"
msgstr "Comentaris"

#: discover/qml/ApplicationPage.qml:692
#, kde-format
msgid "Loading reviews for %1"
msgstr "Es carreguen les ressenyes de %1"

#: discover/qml/ApplicationPage.qml:700
#, kde-format
msgctxt "@info placeholder message"
msgid "Reviews for %1 are temporarily unavailable"
msgstr "Els comentaris per a %1 no són disponibles temporalment"

#: discover/qml/ApplicationPage.qml:724
#, kde-format
msgctxt "@action:button"
msgid "Show All Reviews"
msgstr "Mostra tots els comentaris"

#: discover/qml/ApplicationPage.qml:736
#, kde-format
msgid "Write a Review"
msgstr "Escriviu un comentari"

#: discover/qml/ApplicationPage.qml:736
#, kde-format
msgid "Install to Write a Review"
msgstr "Instal·leu-la per a escriure un comentari"

#: discover/qml/ApplicationPage.qml:748
#, kde-format
msgid "Get Involved"
msgstr "Col·laboreu-hi"

#: discover/qml/ApplicationPage.qml:790
#, kde-format
msgid "Donate"
msgstr "Donatius"

#: discover/qml/ApplicationPage.qml:791
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr "Ajudeu i agraïu als desenvolupadors fent una donació al projecte"

#: discover/qml/ApplicationPage.qml:807
#, kde-format
msgid "Report Bug"
msgstr "Informa d'un error"

#: discover/qml/ApplicationPage.qml:808
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr "Enregistreu una incidència que heu trobat per a ajudar a solucionar-la"

#: discover/qml/ApplicationPage.qml:824
#, kde-format
msgid "Contribute"
msgstr "Col·laboreu-hi"

#: discover/qml/ApplicationPage.qml:825
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr "Ajudeu els desenvolupadors codificant, dissenyant, provant o traduint"

#: discover/qml/ApplicationPage.qml:857
#, kde-format
msgid "All Licenses"
msgstr "Totes les llicències"

#: discover/qml/ApplicationPage.qml:890
#, kde-format
msgid "Content Rating"
msgstr "Valoració del contingut"

#: discover/qml/ApplicationPage.qml:907
#, kde-format
msgid "Risks of proprietary software"
msgstr "Riscos del programari propietari"

#: discover/qml/ApplicationPage.qml:913
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""
"El codi font d'aquesta aplicació està parcialment o totalment tancat a la "
"inspecció i millora pública. Això significa que tercers i usuaris com vós no "
"poden verificar la seva operació, seguretat i fiabilitat, o modificar i "
"redistribuir-la sense el permís exprés dels autors.<nl/><nl/>L'aplicació pot "
"ser perfectament segura d'utilitzar, o pot estar actuant en contra vostra de "
"diverses maneres... com ara recollir la vostra informació personal, seguir "
"la vostra ubicació, o transmetre el contingut dels vostres fitxers a algú "
"altre. No hi ha cap manera fàcil d'estar segur, així que només hauríeu "
"d'instal·lar aquesta aplicació si confieu plenament en els seus autors "
"(<link url='%1'>%2</link>).<nl/><nl/>Podeu aprendre'n més a <link url='%3'>"
"%3</link>."

#: discover/qml/ApplicationPage.qml:914
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""
"El codi font d'aquesta aplicació està parcialment o totalment tancat a la "
"inspecció i millora pública. Això significa que tercers i usuaris com vós no "
"poden verificar la seva operació, seguretat i fiabilitat, o modificar i "
"redistribuir-la sense el permís exprés dels autors.<nl/><nl/>L'aplicació pot "
"ser perfectament segura d'utilitzar, o pot estar actuant en contra vostra de "
"diverses maneres... com ara recollir la vostra informació personal, seguir "
"la vostra ubicació, o transmetre el contingut dels vostres fitxers a algú "
"altre. No hi ha cap manera fàcil d'estar segur, així que només hauríeu "
"d'instal·lar aquesta aplicació si confieu plenament en els seus autors (%1)."
"<nl/><nl/>Podeu aprendre'n més a <link url='%2'>%2</link>."

#: discover/qml/ApplicationsListPage.qml:53
#, kde-format
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "Cerca: %2 - %3 element"
msgstr[1] "Cerca: %2 - %3 elements"

#: discover/qml/ApplicationsListPage.qml:55
#, kde-format
msgid "Search: %1"
msgstr "Cerca: %1"

#: discover/qml/ApplicationsListPage.qml:59
#, kde-format
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%2 - %1 element"
msgstr[1] "%2 - %1 elements"

#: discover/qml/ApplicationsListPage.qml:65
#, kde-format
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "Cerca - %1 element"
msgstr[1] "Cerca - %1 elements"

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:254
#, kde-format
msgid "Search"
msgstr "Cerca"

#: discover/qml/ApplicationsListPage.qml:98 discover/qml/ReviewsPage.qml:99
#, kde-format
msgid "Sort: %1"
msgstr "Ordena: %1"

#: discover/qml/ApplicationsListPage.qml:103
#, kde-format
msgctxt "Search results most relevant to the search query"
msgid "Relevance"
msgstr "Rellevància"

#: discover/qml/ApplicationsListPage.qml:114
#, kde-format
msgid "Name"
msgstr "Nom"

#: discover/qml/ApplicationsListPage.qml:125 discover/qml/Rating.qml:119
#, kde-format
msgid "Rating"
msgstr "Valoració"

#: discover/qml/ApplicationsListPage.qml:136
#, kde-format
msgid "Size"
msgstr "Mida"

#: discover/qml/ApplicationsListPage.qml:147
#, kde-format
msgid "Release Date"
msgstr "Data de publicació"

#: discover/qml/ApplicationsListPage.qml:200
#, kde-format
msgid "Nothing found"
msgstr "No s'ha trobat res"

#: discover/qml/ApplicationsListPage.qml:208
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr "Totes a les categories"

#: discover/qml/ApplicationsListPage.qml:218
#, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "Cerca «%1» al web"

#: discover/qml/ApplicationsListPage.qml:222
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr "https://duckduckgo.com/?q=%1"

#: discover/qml/ApplicationsListPage.qml:233
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr "«%1» no s'ha trobat a la categoria «%2»"

#: discover/qml/ApplicationsListPage.qml:235
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr "«%1» no s'ha trobat a les fonts disponibles"

#: discover/qml/ApplicationsListPage.qml:236
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""
"«%1» pot estar disponible al web. El programari adquirit des del web no ha "
"estat revisat pel vostre distribuïdor per a la funcionalitat o "
"l'estabilitat. Useu-lo amb precaució."

#: discover/qml/ApplicationsListPage.qml:269
#, kde-format
msgid "Still looking…"
msgstr "Encara s'està cercant…"

#: discover/qml/BrowsingPage.qml:20
#, kde-format
msgctxt "@title:window the name of a top-level 'home' page"
msgid "Home"
msgstr "Inici"

#: discover/qml/BrowsingPage.qml:64
#, kde-format
msgid "Unable to load applications"
msgstr "No es poden carregar les aplicacions"

#: discover/qml/BrowsingPage.qml:99
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr "Més popular"

#: discover/qml/BrowsingPage.qml:121
#, kde-format
msgctxt "@title:group"
msgid "Newly Published & Recently Updated"
msgstr "Novetats publicades i actualitzats recentment"

#: discover/qml/BrowsingPage.qml:160
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr "Selecció de l'editor"

#: discover/qml/BrowsingPage.qml:177
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr "Jocs amb la puntuació més alta"

#: discover/qml/BrowsingPage.qml:197 discover/qml/BrowsingPage.qml:228
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr "Vegeu-ne més"

#: discover/qml/BrowsingPage.qml:208
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr "Eines de desenvolupament amb la puntuació més alta"

#: discover/qml/CarouselDelegate.qml:212
#, kde-format
msgctxt "@action:button Start playing media"
msgid "Play"
msgstr "Reprodueix"

#: discover/qml/CarouselDelegate.qml:214
#, kde-format
msgctxt "@action:button Pause any media that is playing"
msgid "Pause"
msgstr "Pausa"

#: discover/qml/CarouselMaximizedViewContent.qml:40
#, kde-format
msgctxt "@action:button"
msgid "Switch to Overlay"
msgstr "Canvia a superposició"

#: discover/qml/CarouselMaximizedViewContent.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Switch to Full Screen"
msgstr "Canvia a pantalla completa"

#: discover/qml/CarouselMaximizedViewContent.qml:75
#, kde-format
msgctxt ""
"@action:button Close overlay/window/popup with carousel of screenshots"
msgid "Close"
msgstr "Tanca"

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Previous Screenshot"
msgstr "Captura de pantalla anterior"

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Next Screenshot"
msgstr "Captura de pantalla següent"

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr "L'execució com a <em>root</em> es desaconsella i és innecessària."

#: discover/qml/DiscoverWindow.qml:58
#, kde-format
msgid "&Home"
msgstr "&Inici"

#: discover/qml/DiscoverWindow.qml:68
#, kde-format
msgid "&Search"
msgstr "&Cerca"

#: discover/qml/DiscoverWindow.qml:76
#, kde-format
msgid "&Installed"
msgstr "&Instal·lat"

#: discover/qml/DiscoverWindow.qml:87
#, kde-format
msgid "Fetching &updates…"
msgstr "Rec&upera les actualitzacions…"

#: discover/qml/DiscoverWindow.qml:87
#, kde-format
msgid "&Update (%1)"
msgid_plural "&Updates (%1)"
msgstr[0] "Act&ualització (%1)"
msgstr[1] "Act&ualitzacions (%1)"

#: discover/qml/DiscoverWindow.qml:95
#, kde-format
msgid "&About"
msgstr "Qu&ant a"

#: discover/qml/DiscoverWindow.qml:103
#, kde-format
msgid "S&ettings"
msgstr "A&rranjament"

#: discover/qml/DiscoverWindow.qml:156 discover/qml/DiscoverWindow.qml:340
#: discover/qml/DiscoverWindow.qml:454
#, kde-format
msgid "Error"
msgstr "Error"

#: discover/qml/DiscoverWindow.qml:160
#, kde-format
msgid "Unable to find resource: %1"
msgstr "No s'ha pogut trobar el recurs: %1"

#: discover/qml/DiscoverWindow.qml:258 discover/qml/SourcesPage.qml:195
#, kde-format
msgid "Proceed"
msgstr "Continua"

#: discover/qml/DiscoverWindow.qml:316
#, kde-format
msgid "Report this issue"
msgstr "Informa d'un problema"

#: discover/qml/DiscoverWindow.qml:340
#, kde-format
msgid "Error %1 of %2"
msgstr "Error %1 de %2"

#: discover/qml/DiscoverWindow.qml:390
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr "Mostra l'anterior"

#: discover/qml/DiscoverWindow.qml:403
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr "Mostra el següent"

#: discover/qml/DiscoverWindow.qml:419
#, kde-format
msgid "Copy to Clipboard"
msgstr "Copia al porta-retalls"

#: discover/qml/Feedback.qml:14
#, kde-format
msgid "Submit usage information"
msgstr "Envia la informació d'ús"

#: discover/qml/Feedback.qml:15
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"Envia informació anònima d'ús al KDE per tal d'entendre millor als nostres "
"usuaris. Per a més informació vegeu https://kde.org/privacypolicy-apps.php."

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Submitting usage information…"
msgstr "S'està enviant la informació d'ús…"

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Configure"
msgstr "Configura"

#: discover/qml/Feedback.qml:23
#, kde-format
msgid "Configure feedback…"
msgstr "Configura els comentaris…"

#: discover/qml/Feedback.qml:30 discover/qml/SourcesPage.qml:22
#, kde-format
msgid "Configure Updates…"
msgstr "Configura les actualitzacions…"

#: discover/qml/Feedback.qml:58
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""
"Podeu ajudar-nos a millorar aquesta aplicació compartint estadístiques i "
"participant en les enquestes."

#: discover/qml/Feedback.qml:58
#, kde-format
msgid "Contribute…"
msgstr "Col·labora…"

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "We are looking for your feedback!"
msgstr "Esperem els vostres comentaris!"

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "Participate…"
msgstr "Participa…"

#: discover/qml/InstallApplicationButton.qml:24
#, kde-format
msgctxt "State being fetched"
msgid "Loading…"
msgstr "S'està carregant…"

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgctxt "@action:button %1 is the name of a software repository"
msgid "Install from %1"
msgstr "Instal·la des de %1"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Install"
msgstr "Instal·la"

#: discover/qml/InstallApplicationButton.qml:32
#, kde-format
msgid "Remove"
msgstr "Elimina"

#: discover/qml/InstalledPage.qml:14
#, kde-format
msgid "Installed"
msgstr "Instal·lat"

#: discover/qml/Navigation.qml:34
#, kde-format
msgid "Resources for '%1'"
msgstr "Recursos per a «%1»"

#: discover/qml/ProgressView.qml:18
#, kde-format
msgid "Tasks (%1%)"
msgstr "Tasques (%1%)"

#: discover/qml/ProgressView.qml:18 discover/qml/ProgressView.qml:45
#, kde-format
msgid "Tasks"
msgstr "Tasques"

#: discover/qml/ProgressView.qml:113
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3, manca %4"

#: discover/qml/ProgressView.qml:121
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:128
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "unknown reviewer"
msgstr "revisor desconegut"

#: discover/qml/ReviewDelegate.qml:66
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> per %2"

#: discover/qml/ReviewDelegate.qml:66
#, kde-format
msgid "Comment by %1"
msgstr "Comentari de %1"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: %1"
msgstr "Versió: %1"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: unknown"
msgstr "Versió: desconeguda"

#: discover/qml/ReviewDelegate.qml:100
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "Vots: %1 de %2"

#: discover/qml/ReviewDelegate.qml:107
#, kde-format
msgid "Was this review useful?"
msgstr "Ha estat útil aquesta ressenya?"

#: discover/qml/ReviewDelegate.qml:119
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "Sí"

#: discover/qml/ReviewDelegate.qml:136
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "No"

#: discover/qml/ReviewDialog.qml:20
#, kde-format
msgid "Reviewing %1"
msgstr "S'està comentant %1"

#: discover/qml/ReviewDialog.qml:27
#, kde-format
msgid "Submit review"
msgstr "Envia el comentari"

#: discover/qml/ReviewDialog.qml:40
#, kde-format
msgid "Rating:"
msgstr "Valoració:"

#: discover/qml/ReviewDialog.qml:45
#, kde-format
msgid "Name:"
msgstr "Nom:"

#: discover/qml/ReviewDialog.qml:53
#, kde-format
msgid "Title:"
msgstr "Títol:"

#: discover/qml/ReviewDialog.qml:73
#, kde-format
msgid "Enter a rating"
msgstr "Introduïu una valoració"

#: discover/qml/ReviewDialog.qml:76
#, kde-format
msgid "Write the title"
msgstr "Escriviu el títol"

#: discover/qml/ReviewDialog.qml:79
#, kde-format
msgid "Write the review"
msgstr "Escriviu el comentari"

#: discover/qml/ReviewDialog.qml:82
#, kde-format
msgid "Keep writing…"
msgstr "Continueu escrivint…"

#: discover/qml/ReviewDialog.qml:85
#, kde-format
msgid "Too long!"
msgstr "Massa llarg!"

#: discover/qml/ReviewDialog.qml:88
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr "Inseriu un nom"

#: discover/qml/ReviewsPage.qml:54
#, kde-format
msgid "Reviews for %1"
msgstr "Ressenyes de %1"

#: discover/qml/ReviewsPage.qml:62
#, kde-format
msgid "Write a Review…"
msgstr "Escriviu un comentari…"

#: discover/qml/ReviewsPage.qml:74
#, kde-format
msgid "Install this app to write a review"
msgstr "Instal·leu aquesta aplicació per a escriure un comentari"

#: discover/qml/ReviewsPage.qml:103
#, kde-format
msgctxt "@label:listbox Most relevant reviews"
msgid "Most Relevant"
msgstr "Més rellevant"

#: discover/qml/ReviewsPage.qml:110
#, kde-format
msgctxt "@label:listbox Most recent reviews"
msgid "Most Recent"
msgstr "Més recent"

#: discover/qml/ReviewsPage.qml:117
#, kde-format
msgctxt "@label:listbox Reviews with the highest ratings"
msgid "Highest Ratings"
msgstr "Valoració més alta"

#: discover/qml/ReviewsStats.qml:53
#, kde-format
msgctxt "how many reviews"
msgid "%1 reviews"
msgstr "%1 comentaris"

#: discover/qml/ReviewsStats.qml:76
#, kde-format
msgctxt "widest character in the language"
msgid "M"
msgstr "M"

#: discover/qml/ReviewsStats.qml:154
#, kde-format
msgid "Unknown reviewer"
msgstr "Revisor desconegut"

# skip-rule: t-com_obe_tan
#: discover/qml/ReviewsStats.qml:175
#, kde-format
msgctxt "Opening upper air quote"
msgid "“"
msgstr "«"

#: discover/qml/ReviewsStats.qml:190
#, kde-format
msgctxt "Closing lower air quote"
msgid "„"
msgstr "»"

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search…"
msgstr "Cerca…"

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search in '%1'…"
msgstr "Cerca a «%1»…"

#: discover/qml/SourcesPage.qml:18
#, kde-format
msgid "Settings"
msgstr "Preferències"

#: discover/qml/SourcesPage.qml:110
#, kde-format
msgid "Default source"
msgstr "Font predeterminada"

#: discover/qml/SourcesPage.qml:118
#, kde-format
msgid "Add Source…"
msgstr "Afegeix una font…"

#: discover/qml/SourcesPage.qml:145
#, kde-format
msgid "Make default"
msgstr "Converteix en el valor predeterminat"

#: discover/qml/SourcesPage.qml:248
#, kde-format
msgid "Increase priority"
msgstr "Augmenta la prioritat"

#: discover/qml/SourcesPage.qml:254
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "Ha fallat en incrementar la preferència «%1»"

#: discover/qml/SourcesPage.qml:260
#, kde-format
msgid "Decrease priority"
msgstr "Disminueix la prioritat"

#: discover/qml/SourcesPage.qml:266
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "Ha fallat en reduir la preferència «%1»"

#: discover/qml/SourcesPage.qml:272
#, kde-format
msgid "Remove repository"
msgstr "Elimina el repositori"

#: discover/qml/SourcesPage.qml:283
#, kde-format
msgid "Show contents"
msgstr "Mostra el contingut"

#: discover/qml/SourcesPage.qml:324
#, kde-format
msgid "Missing Backends"
msgstr "Dorsals que manquen"

#: discover/qml/UpdatesPage.qml:13
#, kde-format
msgid "Updates"
msgstr "Actualitzacions"

#: discover/qml/UpdatesPage.qml:46
#, kde-format
msgid "Update Issue"
msgstr "Actualitza l'assumpte"

#: discover/qml/UpdatesPage.qml:46
#, kde-format
msgid "Technical details"
msgstr "Detalls tècnics"

#: discover/qml/UpdatesPage.qml:62
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""
"Hi ha hagut un problema en instal·lar aquesta actualització. Torneu-ho a "
"provar més tard."

#: discover/qml/UpdatesPage.qml:68
#, kde-format
msgid "See Technical Details"
msgstr "Veure els detalls tècnics"

#: discover/qml/UpdatesPage.qml:95
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid ""
"If the error indicated above looks like a real issue and not a temporary "
"network error, please report it to %1, not KDE."
msgstr ""
"Si l'error indicat anteriorment sembla un problema real i no un error "
"temporal de la xarxa, informeu-ho a %1, no a KDE."

#: discover/qml/UpdatesPage.qml:103
#, kde-format
msgid "Copy Text"
msgstr "Copia el text"

#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid "Error message copied. Remember to report it to %1, not KDE!"
msgstr "S'ha copiat el missatge d'error. Recordeu informar-ho a %1, no a KDE!"

#: discover/qml/UpdatesPage.qml:114
#, kde-format
msgctxt "@action:button %1 is the name of the user's distro/OS"
msgid "Report Issue to %1"
msgstr "Informa del problema a %1"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update the selected items' "
msgid "Update Selected"
msgstr "Actualitza els seleccionats"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update all items'"
msgid "Update All"
msgstr "Actualitza-ho tot"

#: discover/qml/UpdatesPage.qml:180
#, kde-format
msgid "Ignore"
msgstr "Ignora"

#: discover/qml/UpdatesPage.qml:227
#, kde-format
msgid "Select All"
msgstr "Selecciona-ho tot"

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Select None"
msgstr "No seleccionis res"

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Restart automatically after update has completed"
msgstr "Reinicia automàticament després de completar l'actualització"

#: discover/qml/UpdatesPage.qml:249
#, kde-format
msgid "Total size: %1"
msgstr "Mida total: %1"

#: discover/qml/UpdatesPage.qml:284
#, kde-format
msgid "Restart Now"
msgstr "Reinicia ara"

#: discover/qml/UpdatesPage.qml:413
#, kde-format
msgid "Installing"
msgstr "S'està instal·lant"

#: discover/qml/UpdatesPage.qml:444
#, kde-format
msgid "Update from:"
msgstr "Actualitza des de:"

#: discover/qml/UpdatesPage.qml:456
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:463
#, kde-format
msgid "More Information…"
msgstr "Més informació…"

#: discover/qml/UpdatesPage.qml:491
#, kde-format
msgctxt "@info"
msgid "Fetching updates…"
msgstr "S'estan recuperant les actualitzacions…"

#: discover/qml/UpdatesPage.qml:504
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "Actualitzacions"

#: discover/qml/UpdatesPage.qml:513
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr "Cal reiniciar el sistema per a completar el procés d'actualització"

#: discover/qml/UpdatesPage.qml:525 discover/qml/UpdatesPage.qml:532
#: discover/qml/UpdatesPage.qml:539
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "Última versió"

#: discover/qml/UpdatesPage.qml:546
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "Cal comprovar si hi ha actualitzacions"

#: discover/qml/UpdatesPage.qml:553
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr "Es desconeix l'hora de l'última actualització"
