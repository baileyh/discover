# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the discover package.
#
# SPDX-FileCopyrightText: 2023 Guðmundur Erlingsson <gudmundure@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-20 00:38+0000\n"
"PO-Revision-Date: 2023-12-04 09:52+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#: discover/DiscoverObject.cpp:190
#, kde-format
msgctxt "@title %1 is the distro name"
msgid ""
"%1 is not configured for installing apps through Discover—only app add-ons"
msgstr ""
"%1 er ekki grunnstillt til að setja upp forrit í gegnum Discover—einungis "
"forritaviðbætur"

#: discover/DiscoverObject.cpp:192
#, kde-kuit-format
msgctxt "@info:usagetip %1 is the distro name"
msgid ""
"To use Discover for apps, install your preferred module on the "
"<interface>Settings</interface> page, under <interface>Missing Backends</"
"interface>."
msgstr ""
"Til að nota Discover fyrir forrit skaltu setja upp eininguna sem þú vilt á "
"síðunni <interface>Stillingar</interface>, undir<interface>Bakendar sem "
"vantar</interface>."

#: discover/DiscoverObject.cpp:195
#, kde-format
msgctxt "@action:button %1 is the distro name"
msgid "Report This Issue to %1"
msgstr "Tilkynna vandamálið til %1"

#: discover/DiscoverObject.cpp:200
#, kde-kuit-format
msgctxt ""
"@info:usagetip %1 is the distro name; in this case it always contains 'Arch "
"Linux'"
msgid ""
"To use Discover for apps, install <link url='https://wiki.archlinux.org/"
"title/Flatpak#Installation'>Flatpak</link> or <link url='https://wiki."
"archlinux.org/title/KDE#Discover_does_not_show_any_applications'>PackageKit</"
"link> using the <command>pacman</command> package manager.<nl/><nl/> Review "
"<link url='https://archlinux.org/packages/extra/x86_64/discover/'>%1's "
"packaging for Discover</link>"
msgstr ""
"Til að nota Discover fyrir forrit skaltu setja upp <link url='https://wiki."
"archlinux.org/title/Flatpak#Installation'>Flatpak</link> eða <link "
"url='https://wiki.archlinux.org/title/"
"KDE#Discover_does_not_show_any_applications'>PackageKit</link> með "
"<command>pacman</command> pakkastjóranum.<nl/><nl/> Skoðaðu<link "
"url='https://archlinux.org/packages/extra/x86_64/discover/'>pakka fyrir "
"Discover frá %1</link>"

#: discover/DiscoverObject.cpp:291
#, kde-format
msgid "Could not find category '%1'"
msgstr "Fann ekki flokk '%1'"

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr "Reyni að opna skrá '%1' sem er ekki til"

#: discover/DiscoverObject.cpp:328
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""
"Get ekki átt samskipti við flatpak-uppsprettur án flatpak-bakenda %1. Settu "
"hann upp fyrst."

#: discover/DiscoverObject.cpp:332
#, kde-format
msgid "Could not open %1"
msgstr "Gat ekki opnað %1"

#: discover/DiscoverObject.cpp:394
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr "Gakktu úr skugga um að stuðningur við Snap sé uppsettur"

#: discover/DiscoverObject.cpp:396
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""
"Gat ekki opnað %1 því það fannst ekki í neinum tiltækum hugbúnaðarsöfnum."

#: discover/DiscoverObject.cpp:399
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr ""
"Sendu tilkynningu um vandamálið til þeirra sem sjá um pökkun hjá "
"dreifingaraðila þínum."

#: discover/DiscoverObject.cpp:402
#, kde-format
msgid "Report This Issue"
msgstr "Tilkynna vandamálið"

#: discover/DiscoverObject.cpp:464 discover/DiscoverObject.cpp:466
#: discover/main.cpp:117
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/DiscoverObject.cpp:467
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""
"Discover var lokað áður en tiltekin verk kláruðust, bíð eftir að þeim ljúki."

#: discover/main.cpp:33
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr "Opna tilgreinda forritið með appstream://-slóð þess."

#: discover/main.cpp:34
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr "Opna með leitarforriti sem kann á þessa tilteknu mime-gerð."

#: discover/main.cpp:35
#, kde-format
msgid "Display a list of entries with a category."
msgstr "Birta lista yfir færslur með flokk."

#: discover/main.cpp:36
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr "Opna Discover á tilteknu sniði. Sniðin samsvara tækjastikuhnöppunum."

#: discover/main.cpp:37
#, kde-format
msgid "List all the available modes."
msgstr "Telja upp öll tiltæk snið."

#: discover/main.cpp:38
#, kde-format
msgid "Local package file to install"
msgstr "Staðbundin pakkaskrá sem á að setja upp"

#: discover/main.cpp:39
#, kde-format
msgid "List all the available backends."
msgstr "Telja upp alla tiltæka bakenda."

#: discover/main.cpp:40
#, kde-format
msgid "Search string."
msgstr "Leita að streng."

#: discover/main.cpp:41
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Birtir lista yfir tiltæka valkosti fyrir umsagnir notenda"

#: discover/main.cpp:43
#, kde-format
msgid "Supports appstream: url scheme"
msgstr "Styður appstream: slóðaskema"

#: discover/main.cpp:119
#, kde-format
msgid "An application explorer"
msgstr "Forritaskoðari"

#: discover/main.cpp:121
#, fuzzy, kde-format
#| msgid "© 2010-2022 Plasma Development Team"
msgid "© 2010-2024 Plasma Development Team"
msgstr "© 2010-2022 Plasma þróunarteymið"

#: discover/main.cpp:122
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:123
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:124
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "Gæðaprófun, hönnun og nothæfi"

#: discover/main.cpp:128
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "Dan Leinir Turthra Jensen"

#: discover/main.cpp:129
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:136
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Guðmundur Erlingsson"

#: discover/main.cpp:136
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "gudmundure@gmail.com"

#: discover/main.cpp:149
#, kde-format
msgid "Available backends:\n"
msgstr "Tiltækir bakendar:\n"

#: discover/main.cpp:202
#, kde-format
msgid "Available modes:\n"
msgstr "Tiltæk snið:\n"

#: discover/qml/AddonsView.qml:20 discover/qml/Navigation.qml:58
#, kde-format
msgid "Addons for %1"
msgstr "Viðbætur fyrir %1"

#: discover/qml/AddonsView.qml:62
#, kde-format
msgid "More…"
msgstr "Meira…"

#: discover/qml/AddonsView.qml:71
#, kde-format
msgid "Apply Changes"
msgstr "Virkja breytingar"

#: discover/qml/AddonsView.qml:79
#, kde-format
msgid "Reset"
msgstr "Endurstilla"

#: discover/qml/AddSourceDialog.qml:21
#, kde-format
msgid "Add New %1 Repository"
msgstr "Bæta við nýju %1 hugbúnaðarsafni"

#: discover/qml/AddSourceDialog.qml:45
#, kde-format
msgid "Add"
msgstr "Bæta við"

#: discover/qml/AddSourceDialog.qml:50 discover/qml/DiscoverWindow.qml:271
#: discover/qml/InstallApplicationButton.qml:46
#: discover/qml/ProgressView.qml:139 discover/qml/SourcesPage.qml:201
#: discover/qml/UpdatesPage.qml:259 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "Hætta við"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:224
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 einkunn"
msgstr[1] "%1 einkunnir"

#: discover/qml/ApplicationDelegate.qml:177
#: discover/qml/ApplicationPage.qml:224
#, kde-format
msgid "No ratings yet"
msgstr "Engar einkunnir ennþá"

#: discover/qml/ApplicationPage.qml:67
#, kde-format
msgctxt ""
"@item:inlistbox %1 is the name of an app source e.g. \"Flathub\" or \"Ubuntu"
"\""
msgid "From %1"
msgstr "Frá %1"

#: discover/qml/ApplicationPage.qml:84
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:203
#, kde-format
msgid "Unknown author"
msgstr "Óþekktur höfundur"

#: discover/qml/ApplicationPage.qml:248
#, kde-format
msgid "Version:"
msgstr "Útgáfa:"

#: discover/qml/ApplicationPage.qml:260
#, kde-format
msgid "Size:"
msgstr "Stærð:"

#: discover/qml/ApplicationPage.qml:272
#, kde-format
msgid "License:"
msgid_plural "Licenses:"
msgstr[0] "Notandaleyfi:"
msgstr[1] "Notandaleyfi:"

#: discover/qml/ApplicationPage.qml:280
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr "Óþekkt"

#: discover/qml/ApplicationPage.qml:314
#, kde-format
msgid "What does this mean?"
msgstr "Hvað þýðir þetta?"

#: discover/qml/ApplicationPage.qml:323
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] "Meiri upplýsingar..."
msgstr[1] "Meiri upplýsingar..."

#: discover/qml/ApplicationPage.qml:334
#, kde-format
msgid "Content Rating:"
msgstr "Aldurshæfi efnis:"

#: discover/qml/ApplicationPage.qml:343
#, kde-format
msgid "Age: %1+"
msgstr "Aldurstakmark: %1+"

#: discover/qml/ApplicationPage.qml:363
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr "Nánar..."

#: discover/qml/ApplicationPage.qml:385
#, kde-format
msgctxt "@info placeholder message"
msgid "Screenshots not available for %1"
msgstr "Skjáskot ekki í boði fyrir %1"

#: discover/qml/ApplicationPage.qml:559
#, kde-format
msgid "Documentation"
msgstr "Handbækur"

#: discover/qml/ApplicationPage.qml:560
#, kde-format
msgid "Read the project's official documentation"
msgstr "Lesa opinberar handbækur fyrir verkefnið"

#: discover/qml/ApplicationPage.qml:576
#, kde-format
msgid "Website"
msgstr "Vefsvæði"

#: discover/qml/ApplicationPage.qml:577
#, kde-format
msgid "Visit the project's website"
msgstr "Heimsækja vefsvæði verkefnisins"

#: discover/qml/ApplicationPage.qml:593
#, kde-format
msgid "Addons"
msgstr "Viðbætur"

#: discover/qml/ApplicationPage.qml:594
#, kde-format
msgid "Install or remove additional functionality"
msgstr "Setja upp eða fjarlægja viðbótareiginleika"

#: discover/qml/ApplicationPage.qml:613
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr "Deila"

#: discover/qml/ApplicationPage.qml:614
#, kde-format
msgid "Send a link for this application"
msgstr "Senda tengil á þetta forrit"

#: discover/qml/ApplicationPage.qml:630
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr "Tékkaðu á forritinu %1!"

#: discover/qml/ApplicationPage.qml:650
#, kde-format
msgid "What's New"
msgstr "Hvað er nýtt"

#: discover/qml/ApplicationPage.qml:680
#, kde-format
msgid "Reviews"
msgstr "Umsagnir"

#: discover/qml/ApplicationPage.qml:692
#, kde-format
msgid "Loading reviews for %1"
msgstr "Hleð inn umsögnum fyrir %1"

#: discover/qml/ApplicationPage.qml:700
#, kde-format
msgctxt "@info placeholder message"
msgid "Reviews for %1 are temporarily unavailable"
msgstr "Umsagnir fyrir %1 eru tímabundið ekki í boði"

#: discover/qml/ApplicationPage.qml:724
#, kde-format
msgctxt "@action:button"
msgid "Show All Reviews"
msgstr "Sýna allar umsagnir"

#: discover/qml/ApplicationPage.qml:736
#, kde-format
msgid "Write a Review"
msgstr "Skrifa umsögn"

#: discover/qml/ApplicationPage.qml:736
#, kde-format
msgid "Install to Write a Review"
msgstr "Settu forritið upp til að skrifa umsögn"

#: discover/qml/ApplicationPage.qml:748
#, kde-format
msgid "Get Involved"
msgstr "Taka þátt"

#: discover/qml/ApplicationPage.qml:790
#, kde-format
msgid "Donate"
msgstr "Styrkja"

#: discover/qml/ApplicationPage.qml:791
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr "Sýndu þakklæti þitt og stuðning með því að styrkja verkefnið"

#: discover/qml/ApplicationPage.qml:807
#, kde-format
msgid "Report Bug"
msgstr "Senda villuskýrslu"

#: discover/qml/ApplicationPage.qml:808
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr "Skráðu vandamál sem þú rakst á til að hægt sé að lagfæra það"

#: discover/qml/ApplicationPage.qml:824
#, kde-format
msgid "Contribute"
msgstr "Leggja þitt af mörkum"

#: discover/qml/ApplicationPage.qml:825
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr "Hjálpaðu þróunaraðilunum með því að forrita, hanna, prófa eða þýða"

#: discover/qml/ApplicationPage.qml:857
#, kde-format
msgid "All Licenses"
msgstr "Öll notandaleyfi"

#: discover/qml/ApplicationPage.qml:890
#, kde-format
msgid "Content Rating"
msgstr "Aldurshæfi efnis"

#: discover/qml/ApplicationPage.qml:907
#, kde-format
msgid "Risks of proprietary software"
msgstr "Áhætta vegna séreignarhugbúnaðar"

#: discover/qml/ApplicationPage.qml:913
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""
"Frumkóði þessa forrits er lokaður að hluta til eða að öllu leyti og hvorki "
"leyfilegt að skoða hann né breyta honum. Það þýðir að þriðju aðilar og "
"notendur eins og þú geta ekki sannreynt virkni þess, öryggi og áreiðanleika, "
"eða breytt því og endurdreift án yfirlýsts samþykkis höfunda þess.<nl/><nl/"
">Forritið kann að vera fullkomlega öruggt í notkun eða það kann að vinna "
"gegn þér á einhvern hátt—svo sem með því að safna persónuupplýsingum þínum, "
"fylgjast með staðsetningu þinni eða senda efni skráa þinna til annarra "
"aðila. Það er ekki hægt að vita það með vissu og því ætti einungis að setja "
"upp forritið ef þú berð fullt traust til höfunda þess (<link url='%1'>%2</"
"link>). <nl/><nl/>Frekari upplýsingar er að finna á <link url='%3'>%3</link>."

#: discover/qml/ApplicationPage.qml:914
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""
"Frumkóði þessa forrits er lokaður að hluta til eða að öllu leyti og hvorki "
"leyfilegt að skoða hann né breyta honum. Það þýðir að þriðju aðilar og "
"notendur eins og þú geta ekki sannreynt virkni þess, öryggi og áreiðanleika, "
"eða breytt því og endurdreift án yfirlýsts samþykkis höfunda þess.<nl/><nl/"
">Forritið kann að vera fullkomlega öruggt í notkun eða það kann að vinna "
"gegn þér á einhvern hátt—svo sem með því að safna persónuupplýsingum þínum, "
"fylgjast með staðsetningu þinni eða senda efni skráa þinna til annarra "
"aðila. Það er ekki hægt að vita það með vissu og því ætti einungis að setja "
"upp forritið ef þú berð fullt traust til höfunda þess (%1). <nl/><nl/"
">Frekari upplýsingar er að finna á <link url='%2'>%2</link>."

#: discover/qml/ApplicationsListPage.qml:53
#, kde-format
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "Leita: %2 - %3 hlutur"
msgstr[1] "Leita: %2 - %3 hlutir"

#: discover/qml/ApplicationsListPage.qml:55
#, kde-format
msgid "Search: %1"
msgstr "Leita: %1"

#: discover/qml/ApplicationsListPage.qml:59
#, kde-format
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%2 - %1 hlutur"
msgstr[1] "%2 - %1 hlutir"

#: discover/qml/ApplicationsListPage.qml:65
#, kde-format
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "Leita - %1 hlutur"
msgstr[1] "Leita - %1 hlutir"

#: discover/qml/ApplicationsListPage.qml:67
#: discover/qml/ApplicationsListPage.qml:254
#, kde-format
msgid "Search"
msgstr "Leita"

#: discover/qml/ApplicationsListPage.qml:98 discover/qml/ReviewsPage.qml:99
#, kde-format
msgid "Sort: %1"
msgstr "Raða: %1"

#: discover/qml/ApplicationsListPage.qml:103
#, kde-format
msgctxt "Search results most relevant to the search query"
msgid "Relevance"
msgstr "Samsvörun"

#: discover/qml/ApplicationsListPage.qml:114
#, kde-format
msgid "Name"
msgstr "Heiti"

#: discover/qml/ApplicationsListPage.qml:125 discover/qml/Rating.qml:119
#, kde-format
msgid "Rating"
msgstr "Einkunn"

#: discover/qml/ApplicationsListPage.qml:136
#, kde-format
msgid "Size"
msgstr "Stærð"

#: discover/qml/ApplicationsListPage.qml:147
#, kde-format
msgid "Release Date"
msgstr "Útgáfudagur"

#: discover/qml/ApplicationsListPage.qml:200
#, kde-format
msgid "Nothing found"
msgstr "Ekkert fannst"

#: discover/qml/ApplicationsListPage.qml:208
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr "Leita í öllum flokkum"

#: discover/qml/ApplicationsListPage.qml:218
#, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "Leita á netinu að \"%1\""

#: discover/qml/ApplicationsListPage.qml:222
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr "https://duckduckgo.com/?q=%1"

#: discover/qml/ApplicationsListPage.qml:233
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr "\"%1\" fannst ekki í flokknum \"%2\""

#: discover/qml/ApplicationsListPage.qml:235
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr "\"%1\" fannst ekki á þeim stöðum sem eru í boði"

#: discover/qml/ApplicationsListPage.qml:236
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""
"\"%1\" er hugsanlega fáanlegt á netinu. Hugbúnaður sem sóttur er á netið "
"hefur ekki verið yfirfarinn af dreifingaraðila þínum hvað varðar virkni og "
"stöðugleika. Notaðu hann með gát."

#: discover/qml/ApplicationsListPage.qml:269
#, kde-format
msgid "Still looking…"
msgstr "Enn að leita..."

#: discover/qml/BrowsingPage.qml:20
#, kde-format
msgctxt "@title:window the name of a top-level 'home' page"
msgid "Home"
msgstr "Heim"

#: discover/qml/BrowsingPage.qml:64
#, kde-format
msgid "Unable to load applications"
msgstr "Gat ekki hlaðið forrit"

#: discover/qml/BrowsingPage.qml:99
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr "Vinsælast"

#: discover/qml/BrowsingPage.qml:121
#, kde-format
msgctxt "@title:group"
msgid "Newly Published & Recently Updated"
msgstr "Nýútgefið og nýuppfært"

#: discover/qml/BrowsingPage.qml:160
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr "Valið af ritstjórn"

#: discover/qml/BrowsingPage.qml:177
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr "Leikir með hæstu einkunn"

#: discover/qml/BrowsingPage.qml:197 discover/qml/BrowsingPage.qml:228
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr "Sjá meira"

#: discover/qml/BrowsingPage.qml:208
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr "Forritunarverkfæri með hæstu einkunn"

#: discover/qml/CarouselDelegate.qml:212
#, kde-format
msgctxt "@action:button Start playing media"
msgid "Play"
msgstr "Spila"

#: discover/qml/CarouselDelegate.qml:214
#, kde-format
msgctxt "@action:button Pause any media that is playing"
msgid "Pause"
msgstr "Á pásu"

#: discover/qml/CarouselMaximizedViewContent.qml:40
#, kde-format
msgctxt "@action:button"
msgid "Switch to Overlay"
msgstr "Skipta í yfirlag á skjá"

#: discover/qml/CarouselMaximizedViewContent.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Switch to Full Screen"
msgstr "Skipta í allan skjáinn"

#: discover/qml/CarouselMaximizedViewContent.qml:75
#, kde-format
msgctxt ""
"@action:button Close overlay/window/popup with carousel of screenshots"
msgid "Close"
msgstr "Loka"

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Previous Screenshot"
msgstr "Fyrri skjámynd"

#: discover/qml/CarouselNavigationButton.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Next Screenshot"
msgstr "Næsta skjámynd"

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr "Mælt er gegn því keyra sem <em>rót</em> og engin þörf er á því. "

#: discover/qml/DiscoverWindow.qml:58
#, kde-format
msgid "&Home"
msgstr "&Heim"

#: discover/qml/DiscoverWindow.qml:68
#, kde-format
msgid "&Search"
msgstr "&Leita"

#: discover/qml/DiscoverWindow.qml:76
#, kde-format
msgid "&Installed"
msgstr "&Uppsett"

#: discover/qml/DiscoverWindow.qml:87
#, kde-format
msgid "Fetching &updates…"
msgstr "Sæki u&ppfærslur..."

#: discover/qml/DiscoverWindow.qml:87
#, kde-format
msgid "&Update (%1)"
msgid_plural "&Updates (%1)"
msgstr[0] "&Uppfærsla (%1)"
msgstr[1] "&Uppfærslur (%1)"

#: discover/qml/DiscoverWindow.qml:95
#, kde-format
msgid "&About"
msgstr "Um &hugbúnaðinn"

#: discover/qml/DiscoverWindow.qml:103
#, kde-format
msgid "S&ettings"
msgstr "St&illingar"

#: discover/qml/DiscoverWindow.qml:156 discover/qml/DiscoverWindow.qml:340
#: discover/qml/DiscoverWindow.qml:454
#, kde-format
msgid "Error"
msgstr "Villa"

#: discover/qml/DiscoverWindow.qml:160
#, kde-format
msgid "Unable to find resource: %1"
msgstr "Fann ekki tilfang: %1"

#: discover/qml/DiscoverWindow.qml:258 discover/qml/SourcesPage.qml:195
#, kde-format
msgid "Proceed"
msgstr "Halda áfram"

#: discover/qml/DiscoverWindow.qml:316
#, kde-format
msgid "Report this issue"
msgstr "Tilkynna vandamálið"

#: discover/qml/DiscoverWindow.qml:340
#, kde-format
msgid "Error %1 of %2"
msgstr "Villa %1 af %2"

#: discover/qml/DiscoverWindow.qml:390
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr "Sýna fyrra"

#: discover/qml/DiscoverWindow.qml:403
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr "Sýna næsta"

#: discover/qml/DiscoverWindow.qml:419
#, kde-format
msgid "Copy to Clipboard"
msgstr "Afrita á klippispjald"

#: discover/qml/Feedback.qml:14
#, kde-format
msgid "Submit usage information"
msgstr "Senda inn notkunarupplýsingar"

#: discover/qml/Feedback.qml:15
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"Sendir nafnlausar notkunarupplýsingar til KDE svo við fáum betri skilning á "
"notendum okkar. Frekari upplýsingar er að finna á https://kde.org/"
"privacypolicy-apps.php."

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Submitting usage information…"
msgstr "Sendi inn notkunarupplýsingar..."

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Configure"
msgstr "Grunnstilla"

#: discover/qml/Feedback.qml:23
#, kde-format
msgid "Configure feedback…"
msgstr "Grunnstilla umsagnir..."

#: discover/qml/Feedback.qml:30 discover/qml/SourcesPage.qml:22
#, kde-format
msgid "Configure Updates…"
msgstr "Grunnstilla uppfærslur..."

#: discover/qml/Feedback.qml:58
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""
"Þú getur hjálpað okkur að bæta þetta forrit með því að deila talnagögnum og "
"taka þátt í könnunum."

#: discover/qml/Feedback.qml:58
#, kde-format
msgid "Contribute…"
msgstr "Leggja þitt af mörkum..."

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "We are looking for your feedback!"
msgstr "Við óskum eftir umsögnum frá þér!"

#: discover/qml/Feedback.qml:63
#, kde-format
msgid "Participate…"
msgstr "Taka þátt..."

#: discover/qml/InstallApplicationButton.qml:24
#, kde-format
msgctxt "State being fetched"
msgid "Loading…"
msgstr "Hleð..."

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgctxt "@action:button %1 is the name of a software repository"
msgid "Install from %1"
msgstr "Setja upp frá %1"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Install"
msgstr "Setja upp"

#: discover/qml/InstallApplicationButton.qml:32
#, kde-format
msgid "Remove"
msgstr "Fjarlægja"

#: discover/qml/InstalledPage.qml:14
#, kde-format
msgid "Installed"
msgstr "Uppsett"

#: discover/qml/Navigation.qml:34
#, kde-format
msgid "Resources for '%1'"
msgstr "Tilföng fyrir '%1'"

#: discover/qml/ProgressView.qml:18
#, kde-format
msgid "Tasks (%1%)"
msgstr "Verk (%1%)"

#: discover/qml/ProgressView.qml:18 discover/qml/ProgressView.qml:45
#, kde-format
msgid "Tasks"
msgstr "Verk"

#: discover/qml/ProgressView.qml:113
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3, %4 eftir"

#: discover/qml/ProgressView.qml:121
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:128
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "unknown reviewer"
msgstr "óþekktur umsagnaraðili"

#: discover/qml/ReviewDelegate.qml:66
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> með %2"

#: discover/qml/ReviewDelegate.qml:66
#, kde-format
msgid "Comment by %1"
msgstr "Athugasemd frá %1"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: %1"
msgstr "Útgáfa: %1"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Version: unknown"
msgstr "Útgáfa: óþekkt"

#: discover/qml/ReviewDelegate.qml:100
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "Atkvæði: %1 af %2"

#: discover/qml/ReviewDelegate.qml:107
#, kde-format
msgid "Was this review useful?"
msgstr "Var þessi umsögn gagnleg?"

#: discover/qml/ReviewDelegate.qml:119
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "Já"

#: discover/qml/ReviewDelegate.qml:136
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "Nei"

#: discover/qml/ReviewDialog.qml:20
#, kde-format
msgid "Reviewing %1"
msgstr "Umsögn um %1"

#: discover/qml/ReviewDialog.qml:27
#, kde-format
msgid "Submit review"
msgstr "Senda inn umsögn"

#: discover/qml/ReviewDialog.qml:40
#, kde-format
msgid "Rating:"
msgstr "Einkunn:"

#: discover/qml/ReviewDialog.qml:45
#, kde-format
msgid "Name:"
msgstr "Nafn:"

#: discover/qml/ReviewDialog.qml:53
#, kde-format
msgid "Title:"
msgstr "Titill:"

#: discover/qml/ReviewDialog.qml:73
#, kde-format
msgid "Enter a rating"
msgstr "Færðu inn einkunn"

#: discover/qml/ReviewDialog.qml:76
#, kde-format
msgid "Write the title"
msgstr "Skrifaðu titil"

#: discover/qml/ReviewDialog.qml:79
#, kde-format
msgid "Write the review"
msgstr "Skrifaðu umsögnina"

#: discover/qml/ReviewDialog.qml:82
#, kde-format
msgid "Keep writing…"
msgstr "Haltu áfram að skrifa..."

#: discover/qml/ReviewDialog.qml:85
#, kde-format
msgid "Too long!"
msgstr "Of langt!"

#: discover/qml/ReviewDialog.qml:88
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr "Settu inn nafn"

#: discover/qml/ReviewsPage.qml:54
#, kde-format
msgid "Reviews for %1"
msgstr "Umsagnir fyrir %1"

#: discover/qml/ReviewsPage.qml:62
#, kde-format
msgid "Write a Review…"
msgstr "Skrifa umsögn..."

#: discover/qml/ReviewsPage.qml:74
#, kde-format
msgid "Install this app to write a review"
msgstr "Settu forritið upp til að skrifa umsögn"

#: discover/qml/ReviewsPage.qml:103
#, kde-format
msgctxt "@label:listbox Most relevant reviews"
msgid "Most Relevant"
msgstr "Mesta samsvörun"

#: discover/qml/ReviewsPage.qml:110
#, kde-format
msgctxt "@label:listbox Most recent reviews"
msgid "Most Recent"
msgstr "Nýlegast"

#: discover/qml/ReviewsPage.qml:117
#, kde-format
msgctxt "@label:listbox Reviews with the highest ratings"
msgid "Highest Ratings"
msgstr "Með hæstu einkunn"

#: discover/qml/ReviewsStats.qml:53
#, kde-format
msgctxt "how many reviews"
msgid "%1 reviews"
msgstr "%1 umsagnir"

#: discover/qml/ReviewsStats.qml:76
#, kde-format
msgctxt "widest character in the language"
msgid "M"
msgstr "M"

#: discover/qml/ReviewsStats.qml:154
#, kde-format
msgid "Unknown reviewer"
msgstr "Óþekktur umsagnaraðili"

#: discover/qml/ReviewsStats.qml:175
#, kde-format
msgctxt "Opening upper air quote"
msgid "“"
msgstr "“"

#: discover/qml/ReviewsStats.qml:190
#, kde-format
msgctxt "Closing lower air quote"
msgid "„"
msgstr "„"

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search…"
msgstr "Leita…"

#: discover/qml/SearchField.qml:26
#, kde-format
msgid "Search in '%1'…"
msgstr "Leita í '%1'..."

#: discover/qml/SourcesPage.qml:18
#, kde-format
msgid "Settings"
msgstr "Stillingar"

#: discover/qml/SourcesPage.qml:110
#, kde-format
msgid "Default source"
msgstr "Sjálfgefinn upprunastaður"

#: discover/qml/SourcesPage.qml:118
#, kde-format
msgid "Add Source…"
msgstr "Bæta við upprunastað..."

#: discover/qml/SourcesPage.qml:145
#, kde-format
msgid "Make default"
msgstr "Gera sjálfgefið"

#: discover/qml/SourcesPage.qml:248
#, kde-format
msgid "Increase priority"
msgstr "Hækka forgang"

#: discover/qml/SourcesPage.qml:254
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "Mistókst að hækka '%1' kjörstillingu"

#: discover/qml/SourcesPage.qml:260
#, kde-format
msgid "Decrease priority"
msgstr "Lækka forgang"

#: discover/qml/SourcesPage.qml:266
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "Mistókst að lækka '%1' kjörstillingu"

#: discover/qml/SourcesPage.qml:272
#, kde-format
msgid "Remove repository"
msgstr "Fjarlægja hugbúnaðarsafn"

#: discover/qml/SourcesPage.qml:283
#, kde-format
msgid "Show contents"
msgstr "Sýna innihald"

#: discover/qml/SourcesPage.qml:324
#, kde-format
msgid "Missing Backends"
msgstr "Bakendar sem vantar"

#: discover/qml/UpdatesPage.qml:13
#, kde-format
msgid "Updates"
msgstr "Uppfærslur"

#: discover/qml/UpdatesPage.qml:46
#, kde-format
msgid "Update Issue"
msgstr "Uppfærsluvandamál"

#: discover/qml/UpdatesPage.qml:46
#, kde-format
msgid "Technical details"
msgstr "Tæknilegar upplýsingar"

#: discover/qml/UpdatesPage.qml:62
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr "Upp kom vandamál við uppsetningu uppfærslunnar. Reyndu aftur síðar."

#: discover/qml/UpdatesPage.qml:68
#, kde-format
msgid "See Technical Details"
msgstr "Sjá Tæknilegar upplýsingar"

#: discover/qml/UpdatesPage.qml:95
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid ""
"If the error indicated above looks like a real issue and not a temporary "
"network error, please report it to %1, not KDE."
msgstr ""
"Ef villan fyrir ofan virðist vera raunverulegt vandamál en ekki tímabundin "
"netvilla skaltu tilkynna hana til %1, ekki KDE."

#: discover/qml/UpdatesPage.qml:103
#, kde-format
msgid "Copy Text"
msgstr "Afrita texta"

#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid "Error message copied. Remember to report it to %1, not KDE!"
msgstr "Villuboð afrituð. Mundu að senda tilkynningu til %1, ekki KDE!"

#: discover/qml/UpdatesPage.qml:114
#, kde-format
msgctxt "@action:button %1 is the name of the user's distro/OS"
msgid "Report Issue to %1"
msgstr "Tilkynna vandamálið til %1"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update the selected items' "
msgid "Update Selected"
msgstr "Uppfærsla valin"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update all items'"
msgid "Update All"
msgstr "Uppfæra allt"

#: discover/qml/UpdatesPage.qml:180
#, kde-format
msgid "Ignore"
msgstr "Hunsa"

#: discover/qml/UpdatesPage.qml:227
#, kde-format
msgid "Select All"
msgstr "Velja allt"

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Select None"
msgstr "Velja ekkert"

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Restart automatically after update has completed"
msgstr "Endurræsa sjálfkrafa þegar uppfærslunni lýkur"

#: discover/qml/UpdatesPage.qml:249
#, kde-format
msgid "Total size: %1"
msgstr "Heildarstærð: %1"

#: discover/qml/UpdatesPage.qml:284
#, kde-format
msgid "Restart Now"
msgstr "Endurræsa núna"

#: discover/qml/UpdatesPage.qml:413
#, kde-format
msgid "Installing"
msgstr "Set upp"

#: discover/qml/UpdatesPage.qml:444
#, kde-format
msgid "Update from:"
msgstr "Uppfæra frá:"

#: discover/qml/UpdatesPage.qml:456
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:463
#, kde-format
msgid "More Information…"
msgstr "Frekari upplýsingar..."

#: discover/qml/UpdatesPage.qml:491
#, kde-format
msgctxt "@info"
msgid "Fetching updates…"
msgstr "Sæki uppfærslur..."

#: discover/qml/UpdatesPage.qml:504
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "Uppfærslur"

#: discover/qml/UpdatesPage.qml:513
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr "Endurræstu tölvuna til að ljúka uppfærslunni"

#: discover/qml/UpdatesPage.qml:525 discover/qml/UpdatesPage.qml:532
#: discover/qml/UpdatesPage.qml:539
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "Allt uppfært"

#: discover/qml/UpdatesPage.qml:546
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "Ættir að athuga með uppfærslur"

#: discover/qml/UpdatesPage.qml:553
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr "Tími síðustu uppfærslu óþekktur"

#~ msgid "Compact Mode (auto/compact/full)."
#~ msgstr "Þétt snið (sjálfvirkt/þétt/fullt)"

#~ msgid "%1"
#~ msgstr "%1"

#~ msgid ""
#~ "Discover currently cannot be used to install any apps or perform system "
#~ "updates because none of its app backends are available."
#~ msgstr ""
#~ "Sem stendur er ekki hægt að nota Discover til að setja upp forrit eða "
#~ "framkvæma kerfisuppfærslur þar sem enginn forritabakendi er tiltækur."

#~ msgctxt "@info"
#~ msgid ""
#~ "You can install some on the Settings page, under the <interface>Missing "
#~ "Backends</interface> section.<nl/><nl/>Also please consider reporting "
#~ "this as a packaging issue to your distribution."
#~ msgstr ""
#~ "Þú getur sett upp bakenda á stillingasíðunni, undir <interface>Bakendar "
#~ "sem vantar</interface>.<nl/><nl/>Íhugaðu einnig hvort rétt sé að tilkynna "
#~ "þetta pakkavandamál til dreifingaraðila þíns."

#~ msgctxt "@info"
#~ msgid ""
#~ "You can use <command>pacman</command> to install the optional "
#~ "dependencies that are needed to enable the application backends.<nl/><nl/"
#~ ">Please note that Arch Linux developers recommend using <command>pacman</"
#~ "command> for managing software because the PackageKit backend is not well-"
#~ "integrated on Arch Linux."
#~ msgstr ""
#~ "Þú getur notað <command>pacman</command> til að setja upp valkvæð "
#~ "kerfisákvæði sem þarf til að gera forritsbakendana virka.<nl/><nl/"
#~ ">Athugaðu að þróunaraðilar Arch Linux mæla með því að nota "
#~ "<command>pacman</command> til hafa umsjón með hugbúnaði þar sem "
#~ "PackageKit-bakendinn er ekki vel samhæfður við Arch Linux."

#~ msgid "Learn More"
#~ msgstr "Meiri upplýsingar"
